"Logs stuff but in a pretty way."
import inspect
from colored import fg, attr


class Logger():
    "The main Logger class"
    def __init__(self, lvl):
        self.lvl = lvl
        self.version="1.1.1"
    
    def log(self, msg, lvl):
        "Displays a message with different formatting based on provided level."
        
        callstack = inspect.stack()
        # Detect if we came from a wrapper and remove its entry
        if callstack[1][1] == __file__ and callstack[1][3] in ['dbg', 'info', 'norm', 'warn', 'err', 'crit']:
            callstack.remove(callstack[1])

        # for c in callstack:
        #     print(c)

        caller = callstack[1][3]
        cline = callstack[1][2]
        
        msg = str(msg)
        if lvl == -1:
            #Raw
            print(msg)
            return True
        if lvl == 0:
            #Debug
            if self.lvl <= 0:
                for line in msg.split('\n'):
                    print(f"{attr('bold')}[x]{attr(0)} {caller}[{cline}]: {line}")
            return True
        if lvl == 1:
            #Info
            if self.lvl <= 1:
                for line in msg.split('\n'):
                    print(f"{attr('bold')}{fg('blue')}[i]{attr(0)} {fg('blue')}{caller}[{cline}]: {line}{attr(0)}")
            return True
        if lvl == 2:
            #Normal
            if self.lvl <= 2:
                for line in msg.split('\n'):
                    print(f"{attr('bold')}[+]{attr('reset')} {caller}[{cline}]: {line}")
            return True
        if lvl == 3:
            #Warning
            if self.lvl <= 3:
                for line in msg.split('\n'):
                    print(f"{fg('yellow')}{attr('bold')}[!]{attr(0)} {fg('yellow')}{caller}[{cline}]: {line}{attr(0)}")
            return True
        if lvl == 4:
            #Error
            if self.lvl <= 4:
                for line in msg.split('\n'):
                    print(f"{fg('red')}{attr('bold')}[-]{attr(0)} {fg('red')}{caller}[{cline}]: {line}{attr(0)}")
            return True
        if lvl == 5:
            #Critical
            if self.lvl <= 5:
                for line in msg.split('\n'):
                    print(f"{fg('red')}{attr('bold')}{attr('blink')}[*]{attr(0)} {fg('red')}{attr('underlined')}{caller}[{cline}]: {line}{attr(0)}")
            return True
        
    def dbg(self, msg):
        "Debug wrapper"
        self.log(msg, 0)
    def info(self, msg):
        "Info wrapper"
        self.log(msg, 1)
    def norm(self, msg):
        "Normal wrapper"
        self.log(msg, 2)
    def warn(self, msg):
        "Warning wrapper"
        self.log(msg, 3)
    def err(self, msg):
        "Error wrapper"
        self.log(msg, 4)
    def crit(self, msg):
        "Critical wrapper"
        self.log(msg, 5)
        
if __name__ == "__main__":
    myLogger = Logger(0)
    log = myLogger.log
    #code.interact(local=locals())
    myLogger.warn("test")
    log("-1: Raw", -1)
    log(" 0: Debug", 0)
    log(" 1: Info", 1)
    log(" 2: Normal", 2)
    log(" 3: Warning", 3)
    log(" 4: Error", 4)
    log(" 5: Critical", 5)
    myLogger.dbg("Debug Wrapper")
    myLogger.info("Info Wrapper")
    myLogger.norm("Normal Wrapper")
    myLogger.warn("Warning Wrapper")
    myLogger.err("Error Wrapper")
    myLogger.crit("Critical Wrapper")
    myLogger.dbg(1)
    myLogger.dbg(0.0)
    myLogger.dbg(None)
    myLogger.dbg(False)
    myLogger.dbg(True)