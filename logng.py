#!/usr/bin/env python
"""Just a small logging library"""
__author__ =        "Evan Pickard"
__copyright__ =     "Nah"
__credits__ = ["Evan Pickard",]
__license__ = "WTFPL 2"
__version__ = "2.0.1"
__maintainer__ = "Evan Pickard"
__email__ = "expickard@pm.me"
__status__ = "Experimental"

#### Log levels
# 0 - Trace         [#] Intended for raw data
# 1 - Debug         [.] Intended for extra detail just short of dumping raw data
# 2 - Info          [+] Intended for normal logging messages
# 3 - Warning       [ ] Intended for noncritical warning information
# 4 - Error         [-] Intended for error situations
# 5 - Critical      [!] Intended for critical failures that generally result in a crash/force quit/bail

import inspect
import code
from colored import fg, attr

class Logger:
    def __init__(self, level, stackDepth=1, showCaller=True, showLineno=True,
                 showFileName=False, shortFileNames=True, showFuncName=True):
        self.version = "2.0.0"
        self.level = level
        self.stackDepth = stackDepth            # 0: No stack. 1: Show caller. 2+: Show more of the stack
        self.showCaller = showCaller            # Show info about the caller context
        self.showLineno = showLineno            # Show line number?
        self.showFileName = showFileName        # Show file names?
        self.shortFileNames = shortFileNames    # Only use the filename, or use the full path?
        self.showFuncName = showFuncName        # Show the function name?

        self.bold = f"{attr(1)}"                # Bold terminal code
        self.unbold = f"{attr(22)}"             # Unbold and unfaint terminal code
        self.clear = f"{attr(0)}"               # Clear formatting terminal code
        self.blink = f"{attr(5)}"               # Cancer ANSI code
        self.unblink = f"{attr(25)}"            # Make it stop
        self.levels = {                         # Controlling the formatting here
            "0": {
                "name": "trace",
                "icon": f"{self.bold}[#]{self.unbold}",
                "format": f"{fg(8)}"
            },
            "1": {
                "name": "debug",
                "icon": f"{self.bold}[.]{self.unbold}",
                "format": f"{fg('green')}"
            },    
            "2": {
                "name": "info",
                "icon": f"{self.bold}[+]{self.unbold}",
                "format": ""
            },               
            "3": {
                "name": "warning",
                "icon": f"{self.bold}[ ]{self.unbold}",
                "format": f"{fg('yellow')}"
            },            
            "4": {
                "name": "error",
                "icon": f"{self.bold}[-]{self.unbold}",
                "format": f"{fg('red')}"
            },
            "5": {
                "name": "critical",
                "icon": f"{self.bold}{self.blink}[!]{self.unblink}{self.unbold}",
                "format": f"{fg('red')}"
            }     
        }
    
    def formatStack(self, stack):
        "Formats the stack in a sane way"
        # Each Frame object consists of:
        # [0] "frame"
        # [1] "filename"
        # [2] "lineno"
        # [3] "function" 
        # [4] "code_context"
        # [5] "index"
        outlist = []
        out = ""
        
        loops = min(self.stackDepth, len(stack)-1)
        # print(f"Formatting stack to {loops} depth.")
        for i in range(1, loops + 1):
            # This will iterate up through the stack, pulling call info as it goes
            # It will only iterate a maximum of self.stackDepth or len(stack[1:]) times, whichever is lowest
            
            # Skip this item if it's a wrapper, no need to show that
            # TODO: Inspect Logger and match the list against member functions, duh
            if stack[i][1] == __file__ and stack[i][3] in ['trace', 'dbg', 'info', 'warn', 'err', 'crit']:
                continue

            # Handle line number
            if self.showLineno:
                lineno = f"{stack[i][2]}"
            else: 
                lineno = ""

            # Handle function name
            if self.showFuncName:
                function = stack[i][3]
                function += ':'
            else:
                function = ""

            # Handle filename
            if self.showFileName: 
                filename = stack[i][1]
                filename += ':'
            else: 
                filename = ""
            if self.shortFileNames and self.showFileName:
                filename = filename.split('/')[-1:][0]

            outlist.append(f"[{filename}{function}{lineno}]")

        if self.stackDepth > 1:
            outlist.reverse()
            for caller in outlist:
                out += f"{caller}=>"
            out += " "
        else:
            if len(outlist) == 0:
                outlist.append(f"[module]")
            out = f"{outlist[0]}=> "
        return out

    def log(self, msg, level=-1):
        "Log a message. Do some call stack inspection to be clever."
        if level < 0:
            level = self.level
        if level > 5:
            print(f"Invalid log level: {level}")
            return
        callstack = inspect.stack()         

        if self.showCaller:
            callerLine = self.formatStack(callstack)# Get a formatted callstack line
        else:
            callerLine = ""

        lvlformat = self.levels.get(f"{level}")
        out = f"{lvlformat['format']}{lvlformat['icon']} {callerLine}{msg}{self.clear}"
        print(out)

    def trace(self, msg):
        "Wrapper for Logger.log(msg, 0)"
        self.log(msg, 0)
    def dbg(self, msg):
        "Wrapper for Logger.log(msg, 1)"
        self.log(msg, 1)
    def info(self, msg):
        "Wrapper for Logger.log(msg, 2)"
        self.log(msg, 2)
    def warn(self, msg):
        "Wrapper for Logger.log(msg, 3)"
        self.log(msg, 3)
    def err(self, msg):
        "Wrapper for Logger.log(msg, 4)"
        self.log(msg, 4)
    def crit(self, msg):
        "Wrapper for Logger.log(msg, 5)"
        self.log(msg, 5)

def testStack1():
    testStack2()

def testStack2():
    testStack3()

def testStack3():
    Log.log("Test", 3)

def test():
    global Log

    print("Testing levels")
    Log = Logger(0, showCaller=True, showFuncName=True)
    Log.log("Trace", 0)
    Log.log("Debug", 1)
    Log.log("Info", 2)
    Log.log("Warning", 3)
    Log.log("Error", 4)
    Log.log("Critical", 5)

    print("\nTesting all inspect features on")
    Log = Logger(0, stackDepth=10, showCaller=True, showFileName=True, shortFileNames=True, showFuncName=True)
    testStack1()

    print("\nTesting all inspect features on except stack depth")
    Log = Logger(0, stackDepth=1, showCaller=True, showFileName=True, shortFileNames=True, showFuncName=True)
    testStack1()

    print("\nTesting only  func and lineno, stackdepth 1")
    Log = Logger(0, stackDepth=1, showCaller=True, showFileName=False, showFuncName=True)
    testStack1()

    print("\nTesting only func and lineno, stackdepth 10")
    Log = Logger(0, stackDepth=10, showCaller=True, showFileName=False, showFuncName=True)
    testStack1()

    print("\nTesting wrappers being filtered out of call stack")
    Log = Logger(0, stackDepth=10, showCaller=True, showFileName=False, showFuncName=True)
    Log.trace("Trace")
    Log.dbg("Debug")
    Log.info("Info")
    Log.warn("Warn")
    Log.err("Error")
    Log.crit("Crit")


if __name__ == "__main__":
    # Log = Logger(0, showCaller=True, showFileName=True, stackDepth=5, showFuncName=True)
    # Log.log("Trace", 0)
    # Log.log("Debug", 1)
    # Log.log("Info", 2)
    # Log.log("Warn", 3)
    # Log.log("Error", 4)
    # Log.log("Crit", 5)
    # testStack1()
    Log = None
    test()